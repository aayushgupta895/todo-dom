let currentDate = new Date();
let fullDateString = currentDate.toISOString().split("T")[0];

let index;
let arr = [];

window.addEventListener("load", loadData);
const submit = document.querySelector(".submit");
submit.addEventListener("click", add);

function loadData() {
  let storedArr = localStorage.getItem("tasks");
  if (!storedArr) return;
  storedArr = JSON.parse(storedArr)[fullDateString];
  const tempArr = [];
  if (storedArr.length == 0) return;
  storedArr.forEach((element, index) => {
    tempArr.push(element);
    makeTemplate({ value: element }, index, tempArr);
  });
}

function add() {
  console.log(`here inside add`);
  let input = document.querySelector("#inputTask");
  console.log(input.value.trim());
  if (input.value.trim() == "") {
    return;
  }
  arr.push(input.value.trim());
  index = arr.length - 1;
  makeTemplate(input, index, arr);

  localStorage.setItem(
    "tasks",
    JSON.stringify({
      [fullDateString]: arr,
    })
  );
}

function makeTemplate(input, index, arr) {
  let tasks = document.querySelector(".tasks");
  if (tasks.innerHTML == "") {
    const instruction = document.querySelector(".instruction");
    instruction.style.display = "none";
  }
  const items = document.createElement("div");
  const inputValue = input.value.trim();
  let id;
  if (inputValue.length < 7) {
    id = inputValue + arr.length;
  } else {
    id = inputValue.substring(4, 7).trim() + inputValue.substring(2, 6).trim() + arr.length;
  }
  items.innerHTML = `
            <input type="checkbox" id="myCheckbox-${id}">
             <div class="task uncheck">${input.value.trim()}</div>
            <button class="delete-btn">Delete</button>
            <button class="edit-btn">Edit</button>
            <button class="save-btn">Save</button>
        `;
  tasks.appendChild(items);
  const task = items.querySelector(".task");

  items.querySelector(".delete-btn").addEventListener("click", function () {
    tasks.removeChild(items);
    let ind;
    arr.forEach((element, index) => {
      if (element == task.textContent) {
        ind = index;
      }
    });
    arr.splice(ind, 1);
    localStorage.setItem(
      "tasks",
      JSON.stringify({
        [fullDateString]: arr,
      })
    );
  });

  items.querySelector(".edit-btn").addEventListener("click", function () {
    task.contentEditable = true;
    task.style.border = "1px solid black";
  });

  items.querySelector(".save-btn").addEventListener("click", function () {
    task.style.border = "none";
    const str = task.textContent;
    arr[index] = task.textContent;
    localStorage.setItem(
      "tasks",
      JSON.stringify({
        [fullDateString]: arr,
      })
    );
    task.contentEditable = false;
  });

  let checkbox = document.getElementById(`myCheckbox-${id}`);
  checkbox.addEventListener("change", function () {
    if (checkbox.checked) {
      task.classList.remove("uncheck");
      task.classList.add("check");
    } else {
      task.classList.remove("check");
      task.classList.add("uncheck");
    }
  });
  input.value = "";
}

///////////////filters///////////////////////

const completed = document.querySelector(".done");
completed.addEventListener("click", done);
const remaining = document.querySelector(".todo");
remaining.addEventListener("click", todo);
const allTask = document.querySelector(".all");
allTask.addEventListener("click", all);
const removeAll = document.querySelector(".clearAll");
removeAll.addEventListener("click", clearAll);

function done() {
  let tasks = document.querySelector(".tasks");
  for (let node of tasks.childNodes) {
    if (node.childNodes[3].classList.contains("uncheck")) {
      node.style.display = "none";
    } else {
      node.style.display = "flex";
    }
  }
}

function todo() {
  let tasks = document.querySelector(".tasks");
  for (let node of tasks.childNodes) {
    if (node.childNodes[3].classList.contains("check")) {
      node.style.display = "none";
    } else {
      node.style.display = "flex";
    }
  }
}

function all() {
  let tasks = document.querySelector(".tasks");
  for (let node of tasks.childNodes) {
    node.style.display = "flex";
  }
}

function clearAll() {
  let tasks = document.querySelector(".tasks");
  tasks.innerHTML = "";
  const instruction = document.querySelector(".instruction");
  instruction.style.display = "block";
  localStorage.setItem(
    "tasks",
    JSON.stringify({
      [fullDateString]: [],
    })
  );
}
 